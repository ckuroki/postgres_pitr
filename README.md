# Postgres PITR

## Requirements 

Docker

## Execution 

### 1. Build Image

Execute `build.sh`

### 2. Start Container

Execute `run.sh`

Note that the window will display a shell, don't close that window.

### 3. Connect to running container

#### 3.1 Identify running container ID

`docker ps`

Example Output
```
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS               NAMES
b11fb4c6777b        postgres_pitr:latest   "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        5432/tcp            dreamy_hypatia
```

#### 3.2 Connect to running container

`docker exec -it b11fb4c6777b bash`

### 4 Into the container, execute in sequence , this commands

#### 4.1 Switch to postgres user

`su - postgres`   # Switch to postgres user

#### 4.2 Initialize db

`/scripts/initdb.sh`

```
$ /scripts/initdb.sh
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locales
  COLLATE:  C
  CTYPE:    C.UTF-8
  MESSAGES: C
  MONETARY: C
  NUMERIC:  C
  TIME:     C
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/data ... ok
creating subdirectories ... ok
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default timezone ... UTC
selecting dynamic shared memory implementation ... posix
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... sh: locale: not found
2020-01-07 18:20:04.626 UTC [19] WARNING:  no usable system locales were found
ok
syncing data to disk ... ok

WARNING: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    pg_ctl -D /var/lib/postgresql/data -l logfile start

```

#### 4.3 Launch db server 

`/scripts/startdb.sh`

```
b11fb4c6777b:~$ /scripts/startdb.sh
waiting for server to start....2020-01-07 18:20:50.962 UTC [24] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2020-01-07 18:20:50.962 UTC [24] LOG:  listening on IPv6 address "::", port 5432
2020-01-07 18:20:50.975 UTC [24] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2020-01-07 18:20:51.002 UTC [25] LOG:  database system was shut down at 2020-01-07 18:20:05 UTC
2020-01-07 18:20:51.009 UTC [24] LOG:  database system is ready to accept connections
 done
server started
```

#### 4.3 Start replica

`/scripts/start_replica.sh` 

This step creates, replication slot,  launch pg_receivewal,  and configure db to sync replication. Also creates a table to store some data.

```
b11fb4c6777b:/scripts$ ./start_replica.sh
 pg_create_physical_replication_slot

 (replica,)
(1 row)

pg_receivewal: starting log streaming at 0/1000000 (timeline 1)
ALTER SYSTEM
server signaled
2020-01-07 18:21:24.868 UTC [24] LOG:  received SIGHUP, reloading configuration files
2020-01-07 18:21:24.868 UTC [24] LOG:  parameter "synchronous_standby_names" changed to "pg_receivewal"
2020-01-07 18:21:24.963 UTC [38] LOG:  standby "pg_receivewal" is now a synchronous standby with priority 1
CREATE TABLE
```

#### 4.4 Add some data

`/scripts/add_data.sh 100`  # Add 100 rows to sample table
`/scripts/add_data.sh 200`  # Add 200 more rows to sample table

After inserting the query returns the current timestamp, that can be used to recovery PITR.

So, store last timestamp , in this case `2020-01-07 18:29:55.722138+00`

```
b11fb4c6777b:/scripts$ ./add_data.sh 100
INSERT 0 100
              now              | count
``````````````````````````````-+``````-
 2020-01-07 18:29:52.792731+00 |   100
(1 row)

b11fb4c6777b:/scripts$ ./add_data.sh 200
INSERT 0 200
              now              | count
``````````````````````````````-+``````-
 2020-01-07 18:29:55.722138+00 |   300
(1 row)
```


#### 4.5 Take a backup

`/scripts/backup.sh` 

This creates a base backup in `/backup` directory.

```
b11fb4c6777b:/scripts$ ./backup.sh
pg_receivewal: finished segment at 0/2000000 (timeline 1)
23868/23868 kB (100%), 1/1 tablespace
pg_receivewal: finished segment at 0/3000000 (timeline 1)
```

#### 4.6 Add more data ( to check that recovery PITR worked )

`/scripts/add_data.sh 1000` # Add 1000 more rows to sample table

```
b11fb4c6777b:/scripts$ ./add_data.sh 1000
INSERT 0 1000
              now              | count
``````````````````````````````-+``````-
 2020-01-07 18:32:14.772731+00 |  1300
(1 row)
```

#### 4.7 Stop pg_receivewal

`/scripts/stop_replica.sh`

```
b11fb4c6777b:/scripts$ ./stop_replica.sh
2020-01-07 18:33:04.242 UTC [38] LOG:  unexpected EOF on standby connection
```

#### 4.8 Stop db

```
b11fb4c6777b:/scripts$ ./stopdb.sh
2020-01-07 18:34:04.146 UTC [24] LOG:  received immediate shutdown request
waiting for server to shut down....2020-01-07 18:34:04.154 UTC [29] WARNING:  terminating connection because of crash of another server process
2020-01-07 18:34:04.154 UTC [29] DETAIL:  The postmaster has commanded this server process to roll back the current transaction and exit, because another server process exited abnormally and possibly corrupted shared memory.
2020-01-07 18:34:04.154 UTC [29] HINT:  In a moment you should be able to reconnect to the database and repeat your command.
2020-01-07 18:34:04.159 UTC [24] LOG:  database system is shut down
 done
server stopped
```

#### 4.9 Restore backup, generate recovery command

This step retrieves base backup and generates a `recovery.conf` file until a specific timestamp, that is passed as a parameter.

Also renames all `.partial` files on `/receivewal`  dir

To get a valid timestamp use the value copied on step 4.4 

`/scripts/restore.sh "2020-01-07 18:29:55.722138+00"`

```
b11fb4c6777b:/scripts$ ./restore.sh "2020-01-07 18:29:55.722138+00"
backup_label
tablespace_map
pg_notify/
pg_logical/

...

pg_dynshmem/
global/pg_control
000000010000000000000002
```

#### 4.10  Recover db ( start the service)

```
`/scripts/startdb`
```

After this point you can check that are 200 rows on `some_data` table.

#### 4.11 Complete recovery session

`/scripts/finish_recovery` 

```
 psql -U postgres -c "select pg_wal_replay_resume()"
 pg_wal_replay_resume

(1 row)
```

#### 4.12 Clean pg_receivewal

Removes already recovered files

`/scripts/clean_receivewal.sh`

#### 4.13 Enable replica again

Note: this script will log some sql errors because will try to create some_data` table and replication slot that already available, but will restart `pg_receivewal` again.
```
$ ./start_replica.sh
2020-01-07 18:51:45.150 UTC [126] ERROR:  replication slot "replica" already exists
2020-01-07 18:51:45.150 UTC [126] STATEMENT:  select pg_create_physical_replication_slot('replica');
ERROR:  replication slot "replica" already exists
pg_receivewal: starting log streaming at 0/3000000 (timeline 2)
2020-01-07 18:51:45.186 UTC [129] LOG:  standby "pg_receivewal" is now a synchronous standby with priority 1
ALTER SYSTEM
2020-01-07 18:51:45.192 UTC [100] LOG:  received SIGHUP, reloading configuration files
server signaled
2020-01-07 18:51:45.209 UTC [133] ERROR:  relation "some_data" already exists
2020-01-07 18:51:45.209 UTC [133] STATEMENT:  create table some_data ( value bigint, ts timestamp default now());
ERROR:  relation "some_data" already exists
```


