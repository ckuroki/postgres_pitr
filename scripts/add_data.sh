#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Usage add_data <# of records>"
    exit 1
fi
psql -U postgres -c "insert into some_data ( value ) select generate_series(1,$1);"
psql -U postgres -c "select now(), count(*) from some_data;"

