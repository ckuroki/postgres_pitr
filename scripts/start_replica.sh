#!/bin/bash
psql -U postgres -c "select pg_create_physical_replication_slot('replica');"
pg_receivewal -D /receivewal -S replica -Z 0 -h 127.0.0.1 -p 5432 -U postgres -w -v --synchronous &
psql -U postgres -c "alter system set synchronous_standby_names = 'pg_receivewal';"
pg_ctl -D /var/lib/postgresql/data reload
psql -U postgres -c "create table some_data ( value bigint, ts timestamp default now());"
