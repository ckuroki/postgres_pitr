#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Usage: restore <until timestamp>"
    exit 1
fi
# Restore db 
tar -C /var/lib/postgresql/data -xzvf /backup/base.tar.gz
# Restore wal logs
tar -C /var/lib/postgresql/data -xzvf /backup/pg_wal.tar.gz
# Restore partials
for f in /receivewal/*.partial
do
  base=$(echo "$f" | sed s/\.partial//g )
  # Checks if file exists without partial extension
  if [ ! -f $base ]
  then
    # Rename .partial to base name
    mv $f $base 
  else
    # Remove partial file
    rm $f  
  fi
done
# Generate restore file
cat << EOF > /var/lib/postgresql/data/recovery.conf
restore_command = 'cp /receivewal/%f %p'
recovery_target_time = '$1'
EOF

