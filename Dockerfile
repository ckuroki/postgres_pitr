FROM postgres:11.6-alpine
RUN apk update; apk add bash
RUN mkdir /backup ; chown -R postgres:postgres /backup
RUN mkdir /receivewal ; chown -R postgres:postgres /receivewal
RUN mkdir /archived ; chown -R postgres:postgres /archived
RUN mkdir /scripts ; chown -R postgres:postgres /scripts
COPY conf/postgresql.conf.org /var/lib/postgresql
ADD scripts /scripts
RUN chmod -R 755 /scripts/*
RUN chown postgres:postgres /var/lib/postgresql/postgresql.conf.org
COPY conf/docker-entrypoint.sh /usr/local/bin 
